--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_id_opostid` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);
