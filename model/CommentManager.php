<?php

namespace OpenClassrooms\Blog\Model;


require_once('model/Manager.php');



class CommentManager extends Manager
{
	/**
	 * Get a list fro all comments
	 *
	 * @param $postId
	 *
	 * @return \PDOStatement
	 */
	public function getComments($postId)
	{
		$db = $this->dbConnect();

		$comments = $db->prepare('SELECT id, author, comment, DATE_FORMAT(comment_date, \'%d/%m/%Y à %Hh%imin%ss\') AS comment_date_fr FROM comments WHERE post_id = ? ORDER BY comment_date DESC');
		$comments->execute(array($postId));

		return $comments;
	}

	/**
	 * Get one comment for id = $id
	 *
	 * @param $id
	 *
	 * @return \PDOStatement
	 */
	public function getComment($id)
	{
		$db = $this->dbConnect();

		$req = $db->prepare('SELECT id, post_id, author, comment, DATE_FORMAT(comment_date, \'%d/%m/%Y à %Hh%imin%ss\') AS comment_date_fr FROM comments WHERE id = ?');

		$req->execute(array($id));

		$comment = $req->fetch();

		return $comment;
	}

	/**
	 * Post one comment and modify the BDD
	 *
	 * @param $postId
	 * @param $author
	 * @param $comment
	 *
	 * @return mixed
	 */
	public function postComment($postId, $author, $comment)
	{
		$db = $this->dbConnect();

		$comments = $db->prepare('INSERT INTO comments(post_id, author, comment, comment_date) VALUES(?, ?, ?, NOW())');

		$affectedLines = $comments->execute(array($postId, $author, $comment));

		return $affectedLines;
	}

	/**
	 * Update a comment
	 *
	 * @param $commentId
	 * @param $author
	 * @param $comment
	 */
	public function updateComment($id, $author, $comment)
	{
		$db = $this->dbConnect();

		$comments = $db->prepare('UPDATE comments SET author = :author, comment = :comment WHERE id = :id ');


		$comments->bindValue(':id', $id);
		$comments->bindValue(':author', $author);
		$comments->bindValue(':comment', $comment);


		$comment = $comments->execute();

	}
}