<?php
require('controler/frontend.php');

try {
	/* SI la variable $_GET['action'] existe */
	if(isset($_GET['action']))
	{
		// SI la variable $_GET['action'] == listPosts
		if($_GET['action'] == 'listPosts')
		{
			listPosts();
		}
		elseif ($_GET['action'] == 'post')
		{
			/* SI $_GET['id'] existe +++ $_GET['id'] est supé. à 0*/
			if(isset($_GET['id']) && $_GET['id'] > 0)
			{
				post();
			}
			else /* ALORS affiche message d'erreur */
			{
				throw new Exception('Aucun identifiant de billet envoyé !!!');
			}
		}
		elseif ($_GET['action'] == 'addComment')
		{
			/* SI $_GET['id'] existe +++ $_GET['id'] est supé. à 0*/
			if(isset($_GET['id']) && $_GET['id'] > 0)
			{
				/* SI $_POST['author'] & $_POST['comment'] ne sont pas vide */
				if(!empty($_POST['author']) && !empty($_POST['comment']))
				{
					addComment($_GET['id'], $_POST['author'], $_POST['comment']);
				}
				else /* ALORS affiche message d'erreur */
				{
					throw new Exception('Tous les champs ne sont pas remplis !!!');
				}
			}
			else /* ALORS affiche message d'erreur */
			{
				throw new Exception('ERREUR : au identifiant de billet envoyé');
			}
		}
		elseif ($_GET['action'] == 'update')
		{
			/* SI $_GET['id'] existe +++ $_GET['id'] est supé. à 0*/
			if(isset($_GET['id']) && $_GET['id'] > 0)
			{
				update($_GET['id']);
			}
			else /* ALORS affiche message d'erreur */
			{
				throw new Exception('ERREUR : au identifiant de commentaire envoyé');
			}
		}
		elseif ($_GET['action'] == 'updateComment')
		{
			/* SI $_GET['id'] existe +++ $_GET['id'] est supé. à 0*/
			if(isset($_GET['id']) && $_GET['id'] > 0)
			{
				/* SI $_POST['author'] & $_POST['comment'] ne sont pas vide */
				if(!empty($_POST['author']) && !empty($_POST['comment']))
				{
					modifyComment($_POST['id'], $_POST['author'], $_POST['comment']);
				}
				else /* ALORS affiche message d'erreur */
				{
					throw new Exception('ERREUR : Tous les champs ne sont pas remplis !!!');
				}
			}
			else /* ALORS affiche message d'erreur */
			{
				throw new Exception('ERREUR : au identifiant de billet envoyé');
			}
		}
	}
	else /* ALORS PAR DEFAUT on execute le controller ListPosts */
	{
		listPosts();
	}
}
catch(Exception $e)
{
	echo 'ERREUR => ' . $e->getMessage();
}