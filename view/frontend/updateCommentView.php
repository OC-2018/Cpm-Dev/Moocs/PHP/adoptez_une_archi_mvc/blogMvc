<?php $title = 'Mon blog'; ?>

<?php ob_start(); ?>

	<h1>Modification du commentaire d'id : <?= $comment['id'] ?></h1>
	<p><a href="../../index.php">Retour à la liste des billets</a></p>

	<!-- ********** AFFICHAGE Du COMMENTAIRES ******** -->
    <div id="comments" class="well">

            <div class="well panel-default">

                <p class="panel-heading">
                    <strong><?= htmlspecialchars($comment['author']) ?></strong>

                </p>

                <p class="panel-body">
					<?= nl2br(htmlspecialchars($comment['comment'])) ?>
                </p>

                <p class="panel-footer">
                <p class="pull-right">
                    Posté le <?= $comment['comment_date_fr'] ?>
                </p>
                </p>

            </div>

    </div>

    <!-- ********** FORMULAIRE DE MODIF COMMENTAIRES  ******** -->
    <form class="well" action="index.php?action=updateComment&amp;id=<?= $_GET['id'] ?>" method="post">
        <div class="form-group">
            <input  class="hidden" type="text"  name="id" value="<?= $_GET['id'] ?>" />
        </div>
        <div class="form-group">
            <label class="control-label" for="author">Auteur :</label><br />
            <input id="author" class="form-control" type="text"  name="author" />
        </div>
        <div class="form-group">
            <label class="control-label" for="comment">Commentaire :</label>
            <textarea id="comment" class="form-control"  name="comment"></textarea>
        </div>
        <div>
            <input class=" btn btn-primary form-control" type="submit" />
        </div>
    </form> <!-- END <form>  -->

	<!-- ********** ENVOI DU CACHE DANS LA VUE ********* -->
<?php $content = ob_get_clean(); ?>

<?php require('template.php');
