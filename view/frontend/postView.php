<?php $title = 'Mon blog'; ?>

<?php ob_start(); ?>

    <h1>Mon super blog !</h1>
    <p><a href="../../index.php">Retour à la liste des billets</a></p>

    <!-- **********  AFFICHAGE DU BILLET ************** -->
    <div class="news">
        <h3>
			<?= htmlspecialchars($post['title']) ?>
            <em>le <?= $post['creation_date_fr'] ?></em>
        </h3>
        <p>
			<?= nl2br(htmlspecialchars($post['content'])) ?>
        </p>
    </div>


            <!-- ********** AFFICHAGE DES COMMENTAIRES ******** -->
        <h2>Commentaires</h2>

    <!-- ********** FORMULAIRE D'AJOUT DE COMMENTAIRES  ******** -->
    <form class="well" action="index.php?action=addComment&amp;id=<?= $post['id'] ?>" method="post">
        <div class="form-group">
            <label class="control-label" for="author">Auteur :</label><br />
            <input id="author" class="form-control" type="text"  name="author" />
        </div>
        <div class="form-group">
            <label class="control-label" for="comment">Commentaire :</label>
            <textarea id="comment" class="form-control"  name="comment"></textarea>
        </div>
        <div>
            <input class=" btn btn-primary form-control" type="submit" />
        </div>
    </form> <!-- END <form>  -->



    <div id="commentsList" class="well">

		<?php /* Début boucle de commentaires */
		while ($comment = $comments->fetch())
		{
			?>

            <div class="well panel-default">

                <p class="panel-heading">
                    <strong><?= htmlspecialchars($comment['author']) ?></strong>
                    (<a href="index.php?action=update&id=<?= $comment['id'] ?>"><em>Modifier</em></a>)
                </p>

                <p class="panel-body">
                    <?= nl2br(htmlspecialchars($comment['comment'])) ?>
                </p>

                <p class="panel-footer">
                    <p class="pull-right">
                        Posté le <?= $comment['comment_date_fr'] ?>
                    </p>
                </p>

            </div>

			<?php
		} /* FIN de la boucle de commentaires */
		?>
    </div>

    <!-- ********** ENVOI DU CACHE DANS LA VUE ********* -->
<?php $content = ob_get_clean(); ?>

<?php require('template.php');
